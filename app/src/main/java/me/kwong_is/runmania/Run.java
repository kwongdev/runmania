package me.kwong_is.runmania;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.LinkedBlockingQueue;

import me.kwong_is.runmania.bluetooth.BluetoothConstants;
import me.kwong_is.runmania.bluetooth.BluetoothLeService;
import org.vinuxproject.sonic.Sonic;

// Main running activity
public class Run extends Activity implements OnClickListener {
	private static final String TAG = "Run";

	private TextView mHeartRateField;
	private TextView mPercentageField;
	private String mDeviceAddress;
	private BluetoothLeService mBluetoothLeService;

	private Button previousButton;
	private Button nextButton;

	private int targetHeartRate;
	private ArrayList<String> musicPaths;
	private ArrayList<Integer> low_heartrate;
	private ArrayList<Integer> on_heartrate;
	private ArrayList<Integer> high_heartrate;
	private int songListPosition;
	private boolean songOver = false;
	private boolean isPaused = false;

	private ArrayList<AsyncTask> array_task = new ArrayList<AsyncTask>(2);
	private LinkedBlockingQueue<byte[]> queue;
	private int samplingFrequency = 44000;
	private int channelCount = 2;
	private float speed = 1.0f;
	private static final float MIN_SPEED = 0.5f;
	private static final float MAX_SPEED = 2.0f;

    // Should be null before onCreate
//    private AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

	//  Code to manage Service lifecycle.
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName, IBinder service) {
			mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
			if (!mBluetoothLeService.initialize()) {
				Log.e(TAG, "Unable to initialize Bluetooth");
				finish();
			}
			// Automatically connects to the device upon successful start-up initialization.
			mBluetoothLeService.connect(mDeviceAddress);
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
		}
	};

	// Handles various events fired by the Service.
	// ACTION_GATT_CONNECTED: connected to a GATT server.
	// ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
	// ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
	// ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
	//                        or notification operations.
	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
				Log.v(TAG, "connected");
			} else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
				Log.v(TAG, "disconnected");
				// attempt to reconnect
				mPercentageField.setText(R.string.runmania_bt_disconnected);
				mBluetoothLeService.connect(mDeviceAddress);
			} else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
				Log.v(TAG, "registering service");
				mPercentageField.setText(R.string.runmania_bt_connecting);
			} else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
				updateDataUI(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
			}
		}
	};

    // Handles if headphones are unplugged (pause to not make it 'noisy')
	private final BroadcastReceiver mNoisyAudioStreamReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
				if (!isPaused) {
					isPaused = true;
					Button button = (Button)findViewById(R.id.runmania_run_play);
					button.setText(R.string.runmania_resume);
				}
			}
		}
	};

    // Handles being able to pause or play the music from headset button (remote)
    // TODO: register and unregister this properly
    private final BroadcastReceiver mRemoteControlReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
                KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                Log.v(TAG, "KeyCode is " + event.getKeyCode());
                if (KeyEvent.KEYCODE_MEDIA_PLAY == event.getKeyCode()) {
//                    isPaused = !isPaused;
                    if (!isPaused) {
                        isPaused = true;
                        Button button = (Button)findViewById(R.id.runmania_run_play);
                        button.setText(R.string.runmania_resume);
                    } else {
                        isPaused = false;
                        Button button = (Button)findViewById(R.id.runmania_run_play);
                        button.setText(R.string.runmania_pause);
                    }
                }
            }
        }
    };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_run);

		View startButton = findViewById(R.id.runmania_run_play);
		startButton.setOnClickListener(this);
		View stopButton = findViewById(R.id.runmania_run_stop);
		stopButton.setOnClickListener(this);
		previousButton = (Button) findViewById(R.id.runmania_run_previous);
		previousButton.setOnClickListener(this);
		nextButton = (Button) findViewById(R.id.runmania_run_next);
		nextButton.setOnClickListener(this);

		mHeartRateField = (TextView) findViewById(R.id.runmania_run_heartrate);
		mPercentageField = (TextView) findViewById(R.id.runmania_run_target);

		final Intent intent = getIntent();
		mDeviceAddress = intent.getStringExtra(BluetoothConstants.EXTRAS_DEVICE_ADDRESS);
		musicPaths = intent.getStringArrayListExtra(RunmaniaConstants.SONG_PATHS);
		low_heartrate = new ArrayList<Integer>(Collections.nCopies(musicPaths.size(), 0));
		on_heartrate = new ArrayList<Integer>(Collections.nCopies(musicPaths.size(), 0));
		high_heartrate = new ArrayList<Integer>(Collections.nCopies(musicPaths.size(), 0));
		songListPosition = 0;

		checkSongListStatus();

		targetHeartRate = intent.getIntExtra(RunmaniaConstants.HEART_RATE, 0);

		Log.v(TAG, "target heart rate is: " + targetHeartRate);
		Log.v(TAG, "got " + musicPaths.size() + " songs"); 

        if (!RunmaniaMenu.DEBUG_MODE) {
            Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
            bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        }


        // Volume controls affect music volume rather than ringer
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

		registerReceiver(mNoisyAudioStreamReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

        // TODO: this might not be correct.
        registerReceiver(mRemoteControlReceiver, new IntentFilter(Intent.ACTION_MEDIA_BUTTON));

	
		// start playing music
		queue = new LinkedBlockingQueue<byte[]>();
		playSong();
	}
	
	private void updateCurrentSong() {
		String path = musicPaths.get(songListPosition).split("/", 2)[1];
		TextView nowPlaying = (TextView) findViewById(R.id.runmania_now_playing_song);
		TextView songCountField = (TextView) findViewById(R.id.runmania_song_count);
		
		MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(path);

		String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
		if (title == null || title.length() == 0) {
            title = new File(path).getName();
        }
		
		String pos = (songListPosition + 1) + " of " + musicPaths.size();
		
		nowPlaying.setText(title);
		nowPlaying.setSelected(true);
		songCountField.setText(pos);
	}

	private void checkSongListStatus() {
		Log.v(TAG, "song position:" + songListPosition);
		if (songListPosition > 0) {
			previousButton.setEnabled(true);
		} else {
			previousButton.setEnabled(false);
			Log.v(TAG, "no more songs previous");
		}

		if (songListPosition < musicPaths.size() - 1) {
			nextButton.setEnabled(true);
		} else {
			nextButton.setEnabled(false);
			Log.v(TAG, "no more songs next");
		}
	}

	private void playSong() {
        if (songListPosition < musicPaths.size()) {
            checkSongListStatus();
            updateCurrentSong();

            String path = musicPaths.get(songListPosition).split("/", 2)[1];

            queue.clear();
            isPaused = false;
            songOver = false;
            array_task.add(new AudioDecoder().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, path));
            array_task.add(new AudioPlayer().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR));
        } else {
            songListPosition--;
            stopSong();
            Intent resultsIntent = new Intent(Run.this, Results.class);
            resultsIntent.putExtras(getIntent());
            resultsIntent.putStringArrayListExtra(RunmaniaConstants.SONG_PATHS, musicPaths);
            resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.LOW_HR, low_heartrate);
            resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.ON_HR, on_heartrate);
            resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.HIGH_HR, high_heartrate);
            startActivity(resultsIntent);
            finish();
        }
	}
	
	private void stopSong() {
		songOver = true;
		
		low_heartrate.set(songListPosition, BluetoothLeService.low_heartrate);
		on_heartrate.set(songListPosition, BluetoothLeService.on_heartrate);
		high_heartrate.set(songListPosition, BluetoothLeService.high_heartrate);
		
		for(int i = 0; i < array_task.size(); i++) {
			array_task.get(i).cancel(true);
		}
		array_task.clear();
        if (!RunmaniaMenu.DEBUG_MODE)
		    mBluetoothLeService.resetHeartRateCounter();
	}

	@Override
	protected void onResume() {
		super.onResume();

        if (!RunmaniaMenu.DEBUG_MODE) {
            registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
            if (mBluetoothLeService != null) {
                final boolean result = mBluetoothLeService.connect(mDeviceAddress);
                Log.d(TAG, "Connect request result=" + result);
            }
        }

	}

	@Override
	protected void onPause() {
		super.onPause();
        if (!RunmaniaMenu.DEBUG_MODE)
            unregisterReceiver(mGattUpdateReceiver);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mNoisyAudioStreamReceiver);
        unregisterReceiver(mRemoteControlReceiver);

        if (!RunmaniaMenu.DEBUG_MODE) {
            unbindService(mServiceConnection);
            mBluetoothLeService.disconnect();
            mBluetoothLeService.close();
            mBluetoothLeService = null;
        }
	}

	private void updateDataUI(String data) {
		if (data != null) {
			mHeartRateField.setText(data + " BPM");

			int percentage = (int) ((Double.valueOf(data)/targetHeartRate) * 100);
			mPercentageField.setText(percentage + "% of target heart rate");
		}
	}
	
	@Override
	public void onBackPressed() {
		stopPrompt();
	}
	
	private void stopPrompt() {
		isPaused = true;

		new AlertDialog.Builder(this)
		.setTitle("Stop workout")
		.setMessage("Are you sure you want to stop running?")
		.setPositiveButton(R.string.runmania_stop_running, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				stopSong();
				
				Intent resultsIntent = new Intent(Run.this, Results.class);
				resultsIntent.putExtras(getIntent());
				resultsIntent.putStringArrayListExtra(RunmaniaConstants.SONG_PATHS, musicPaths);
				resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.LOW_HR, low_heartrate);
				resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.ON_HR, on_heartrate);
				resultsIntent.putIntegerArrayListExtra(RunmaniaConstants.HIGH_HR, high_heartrate);
				startActivity(resultsIntent);
				finish();

			}
		})
		.setNegativeButton(R.string.runmania_keep_running, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) { 
				isPaused = false;
			}
		})
		.setIcon(android.R.drawable.ic_dialog_alert)
		.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_run_previous:
			stopSong();
			--songListPosition;
			checkSongListStatus();
			updateCurrentSong();
			
			if (!isPaused)
				playSong();
			break;
		case R.id.runmania_run_next:
			stopSong();
			++songListPosition;
			checkSongListStatus();
			updateCurrentSong();
			
			if (!isPaused)
				playSong();
			break;
		case R.id.runmania_run_play:
			Button button = (Button)findViewById(R.id.runmania_run_play);
			if (!isPaused) {
				isPaused = true;
				button.setText(R.string.runmania_resume);
			} else if (isPaused && !songOver) {
				isPaused = false;
				button.setText(R.string.runmania_pause);
			} else if (isPaused && songOver) {
				isPaused = false;
				playSong();
				button.setText(R.string.runmania_pause);
			}
			break;
		case R.id.runmania_run_stop:
			stopPrompt();
			
			break;
		}

	}

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
		intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
		return intentFilter;
	}

	/**
	 * Decodes an audio file and pushes the resulting PCM data onto
	 * the common queue.
	 * @author kenny
	 *
	 */
	private class AudioDecoder extends AsyncTask<String, Void, Void> {
		private static final String TAG = "AudioDecoder";

		private MediaExtractor extractor;
		private MediaCodec decoder;

		public Void doInBackground(String ...strings) {
			extractor = new MediaExtractor();

			for (String path : strings) {

				try {
					if (path != null) {
						extractor.setDataSource(path);
					}
				} catch (IOException e) {
					e.printStackTrace(); // could not set data source
				}

				for (int i = 0; i < extractor.getTrackCount(); i++) {
					MediaFormat format = extractor.getTrackFormat(i);
					samplingFrequency = format.getInteger(MediaFormat.KEY_SAMPLE_RATE); // assumes these values
					channelCount = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);    // don't change in a file
					String mime = format.getString(MediaFormat.KEY_MIME);
					if (mime.startsWith("audio/")) {
						extractor.selectTrack(i);
                        try {
                            decoder = MediaCodec.createDecoderByType(mime);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        decoder.configure(format, null, null, 0);
						break;
					}
				}

				if (decoder == null) {
					Log.e(TAG, "Can't find audio info!");
					return null;
				}

				decoder.start();

				ByteBuffer[] inputBuffers = decoder.getInputBuffers();
				ByteBuffer[] outputBuffers = decoder.getOutputBuffers();
				BufferInfo info = new BufferInfo();
				boolean isEOS = false;

				while (true) {
					if (isCancelled()) break;
					
					if (!isEOS) {
						int inIndex = decoder.dequeueInputBuffer(10000);
						if (inIndex >= 0) {
							ByteBuffer buffer = inputBuffers[inIndex];
							int sampleSize = extractor.readSampleData(buffer, 0);
							if (sampleSize < 0) {
								// We shouldn't stop the playback at this point, just pass the EOS
								// flag to decoder, we will get it again from the dequeueOutputBuffer
								Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
								decoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
								isEOS = true;
							} else {
								decoder.queueInputBuffer(inIndex, 0, sampleSize, extractor.getSampleTime(), 0);
								extractor.advance();
							}
						}
					}

					int outIndex = decoder.dequeueOutputBuffer(info, 10000);
					switch (outIndex) {
					case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
						Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
						outputBuffers = decoder.getOutputBuffers();
						break;
					case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
						Log.d(TAG, "New format " + decoder.getOutputFormat());
						break;
					case MediaCodec.INFO_TRY_AGAIN_LATER:
						Log.d(TAG, "dequeueOutputBuffer timed out!");
						break;
					default:
						ByteBuffer buffer = outputBuffers[outIndex];
						byte[] b = new byte[info.size-info.offset];                         
						int a = buffer.position();
						buffer.get(b);
						buffer.position(a);

						queue.offer(b);

						decoder.releaseOutputBuffer(outIndex, true);
						break;
					}

					// All decoded frames have been rendered, we can stop playing now
					if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
						Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
						break;
					}
				}
			}

			decoder.stop();
			decoder.release();
			extractor.release();
			Log.v(TAG, "Finished decoding song to PCM");
			return null;
		}

	}

	private class AudioPlayer extends AsyncTask<Void, Void, Void> {
		private static final String TAG = "AudioPlayer";

		private AudioTrack audiotrack;

		public Void doInBackground(Void ...params) {
			int format = findFormatFromChannels(channelCount);
			int minSize = AudioTrack.getMinBufferSize(samplingFrequency, format, AudioFormat.ENCODING_PCM_16BIT);        
			audiotrack = new AudioTrack(
					AudioManager.STREAM_MUSIC, 
					samplingFrequency, 
					format, 
					AudioFormat.ENCODING_PCM_16BIT, 
					minSize*4,
					AudioTrack.MODE_STREAM);

			Sonic sonic = new Sonic(samplingFrequency, channelCount);
			byte samples[] = new byte[4096];
			byte modifiedSamples[] = new byte[2048];

			try {
				InputStream pcmStream;
				byte[] data;
				
				Thread.sleep(500); // TODO: Adjust as necessary

				while (queue.peek() != null) {
					if (isCancelled()) {
						break;
					}
					
					if (!isPaused && audiotrack.getPlayState() != AudioTrack.PLAYSTATE_PLAYING) {
						Log.v(TAG, "playing");
						audiotrack.play();
					}

					if (isPaused) {
						audiotrack.pause();
						continue;
					}

					if ((data = queue.poll()) != null) {
						pcmStream = new ByteArrayInputStream(data);
						for (int bytesRead = pcmStream.read(samples); bytesRead >= 0; bytesRead = pcmStream.read(samples)) {
							int hrate = (BluetoothLeService.service_heartrate == 0) ? targetHeartRate : BluetoothLeService.service_heartrate;
							speed = Math.max(MIN_SPEED, Math.min(MAX_SPEED, (float) (Double.valueOf(hrate)/targetHeartRate)));
							sonic.setSpeed(speed);

							if(bytesRead > 0) {
								sonic.putBytes(samples, bytesRead);
							} else {
								sonic.flush();
							}
							int available = sonic.availableBytes(); 
							if (available > 0) {
								if(modifiedSamples.length < available) {
									modifiedSamples = new byte[available*2];
								}
								sonic.receiveBytes(modifiedSamples, available);
								audiotrack.write(modifiedSamples, 0, available);
							}
						}
					}
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			audiotrack.pause();
			audiotrack.flush();
			return null;
		}

		public void onPostExecute(Void result) {
			Log.v(TAG, "onPostExecute");
			if (!isCancelled()) {
				++songListPosition;
				playSong();
			} else {
				songOver = true;
			}
		}

		private int findFormatFromChannels(int numChannels) {
			switch(numChannels) {
			case 1: return AudioFormat.CHANNEL_OUT_MONO;
			case 2: return AudioFormat.CHANNEL_OUT_STEREO;
			default: return -1; // Error
			}
		}
	}
}
