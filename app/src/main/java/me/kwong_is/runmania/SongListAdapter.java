package me.kwong_is.runmania;

import android.app.Activity;
import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhaarman.listviewanimations.ArrayAdapter;

import java.io.File;
import java.util.ArrayList;


//Adapter for holding song paths added by the user
public class SongListAdapter extends ArrayAdapter<String> {
    private ArrayList<String> mSongPaths;
    private LayoutInflater mInflator;

    public SongListAdapter(Activity activity, ArrayList<String> songs) {
    	super(songs);
        mSongPaths = songs;
        mInflator = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setSongPaths(ArrayList<String> newPaths) {
        mSongPaths = newPaths;
        notifyDataSetChanged();
    }
    
    @Override
    public long getItemId(final int position) {
        return getItem(position).hashCode();
    }
    
    @Override
    public boolean hasStableIds() {
    	return true;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = mInflator.inflate(R.layout.runmania_musiclist_item, null);
            viewHolder = new ViewHolder();
            viewHolder.songName = (TextView) view.findViewById(R.id.song_name);
            viewHolder.songDuration = (TextView) view.findViewById(R.id.song_duration);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        String songPath = mSongPaths.get(i).split("/", 2)[1];
        
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
		mmr.setDataSource(songPath);

		long durationMs = Long.parseLong(mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
		int durationSec = (int) (durationMs / 1000);

		StringBuilder durationValue = new StringBuilder();
		durationValue.append(durationSec / 60).append(":");
		durationValue.append(String.format("%02d", durationSec % 60));
		
        final String songName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        if (songName != null && songName.length() > 0)
            viewHolder.songName.setText(songName);
        else
            viewHolder.songName.setText(new File(songPath).getName());

        viewHolder.songDuration.setText(durationValue.toString());
        return view;
        
    }
    
    static class ViewHolder {
        TextView songName;
        TextView songDuration;
    }
}
