package me.kwong_is.runmania;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.UUID;

import me.kwong_is.runmania.bluetooth.BluetoothConstants;


public class RunmaniaMenu extends Activity implements OnClickListener {
	public static final String TAG = "RunmaniaMenu";
	
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private boolean mScanning;
    
    private ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<BluetoothDevice>();

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 5 seconds.
    private static final long SCAN_PERIOD = 5000;

    public static final boolean DEBUG_MODE = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_menu);
		
		View runButton = findViewById(R.id.runmania_main_start_button);
		runButton.setOnClickListener(this);
		View acknowledgementsButton = findViewById(R.id.runmania_main_acknowledgements_button);
		acknowledgementsButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.runmania_main_exit_button);
		exitButton.setOnClickListener(this);
		
		mHandler = new Handler();

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
		// fire an intent to display a dialog asking the user to grant permission to enable it.
		if (!mBluetoothAdapter.isEnabled()) {
			if (!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			}
		}
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

	@Override
	protected void onPause() {
		super.onPause();
		
		scanLeDevice(false);
		bluetoothDevices.clear();
	}
	
    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                	mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            
            UUID[] supportedServices = {UUID.fromString(BluetoothConstants.HEART_RATE_SERVICE)};
            mBluetoothAdapter.startLeScan(supportedServices, mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
    		new BluetoothAdapter.LeScanCallback() {

    	@Override
    	public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
    		runOnUiThread(new Runnable() {
    			@Override
    			public void run() {
    				if(!bluetoothDevices.contains(device))
    					bluetoothDevices.add(device);
    			}
    		});
    	}
    };

	public void launchRingDialog() {
		final ProgressDialog ringProgressDialog = ProgressDialog.show(RunmaniaMenu.this, "Please wait ...",	"Detecting heart rate monitor ...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Here you should write your time consuming task...
					// Let the progress ring for 10 seconds...
					bluetoothDevices.clear();
					scanLeDevice(true);
					Thread.sleep(SCAN_PERIOD);
				} catch (Exception e) {

				}
				ringProgressDialog.dismiss();
				Log.v(TAG, "num of heart rate-capable bluetooth devices: " + bluetoothDevices.size());

				tryShowDeviceOptions();
			}
		}).start();
	}

	private void tryShowDeviceOptions() {
		if (bluetoothDevices.isEmpty()) {
			RunmaniaMenu.this.runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(RunmaniaMenu.this, R.string.runmania_no_bt_devices, Toast.LENGTH_SHORT).show();
				}
			});
			return;
		}
        Intent intent = new Intent(this, SelectDevice.class);
        intent.putParcelableArrayListExtra(BluetoothConstants.EXTRAS_DEVICE_LIST, bluetoothDevices);
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        startActivity(intent);
    }
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_main_start_button:
            if (!DEBUG_MODE) {
                launchRingDialog();
            } else {
                Intent intent = new Intent(this, EnterAge.class);
                startActivity(intent);
            }
			break;
		case R.id.runmania_main_acknowledgements_button:
			Intent acknowledgementsIntent = new Intent(this, Acknowledgements.class);
			startActivity(acknowledgementsIntent);
			break;
		case R.id.runmania_main_exit_button:
			finish();
			break;
		}
	}

}