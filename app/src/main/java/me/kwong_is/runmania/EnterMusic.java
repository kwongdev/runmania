package me.kwong_is.runmania;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;
import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.swipedismiss.OnDismissCallback;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import me.kwong_is.runmania.util.SimpleFileDialog;


public class EnterMusic extends Activity implements OnClickListener {
	private static final String TAG = "EnterMusic";
	private ArrayList<String> pathList = new ArrayList<String>();
	private String initialPath;
	DynamicListView musicListView;
	SongListAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_entermusic);

        SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
        Gson gson = new Gson();
        String jsonPath = settings.getString(RunmaniaConstants.SAVED_MUSIC, null);
        Type stringArrayList = new TypeToken<ArrayList<String>>(){}.getType();
        pathList = (jsonPath == null) ? (new ArrayList<String>()) : (ArrayList<String>) gson.fromJson(jsonPath, stringArrayList);
        Log.v(TAG, "onCreate (loading songs), count: " + pathList.size());

		initialPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		Log.v(TAG, "initial path here: " + initialPath);

		View backButton = findViewById(R.id.runmania_entermusic_back);
		backButton.setOnClickListener(this);

		View nextButton = findViewById(R.id.runmania_entermusic_next);
		nextButton.setOnClickListener(this);

		View quitButton = findViewById(R.id.runmania_entermusic_quit);
		quitButton.setOnClickListener(this);

		View addButton = findViewById(R.id.runmania_entermusic_addsongs);
		addButton.setOnClickListener(this);

		musicListView = (DynamicListView) findViewById(R.id.runmania_musicList);

		mAdapter = new SongListAdapter(EnterMusic.this, pathList);
		AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(mAdapter);
		animationAdapter.setAbsListView(musicListView);
		musicListView.setAdapter(animationAdapter);

		musicListView.enableDragAndDrop();
		musicListView.setOnItemLongClickListener(
				new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(final AdapterView<?> parent, final View view,
							final int position, final long id) {
						musicListView.startDragging(position);
						return true;
					}
				}
				);
		musicListView.enableSwipeToDismiss(
				new OnDismissCallback() {
					@Override
					public void onDismiss(@NonNull final ViewGroup listView, @NonNull final int[] reverseSortedPositions) {
						for (int position : reverseSortedPositions) {
							mAdapter.remove(position);
						}
					}
				}
				);

	}

    @Override
    protected void onDestroy() {

        super.onDestroy();
        SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
        Gson gson = new Gson();
        settings.edit().putString(RunmaniaConstants.SAVED_MUSIC, gson.toJson(mAdapter.getItems())).apply();

        Log.v(TAG, "onDestroy (saving music), count: " + mAdapter.getCount());
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_entermusic_addsongs:
			browse(v);
			break;
		case R.id.runmania_entermusic_next:
			if (mAdapter.getCount() <= 0) {
				Toast.makeText(this, "Please add some songs first!", Toast.LENGTH_LONG).show();
				break;
			}

			Intent runIntent = new Intent(this, Run.class);
			runIntent.putExtras(getIntent());
			runIntent.putStringArrayListExtra(RunmaniaConstants.SONG_PATHS, pathList);
			startActivity(runIntent);
			break;
		case R.id.runmania_entermusic_back:
			finish();
			break;
		case R.id.runmania_entermusic_quit:
			Intent intent = new Intent(getApplicationContext(), RunmaniaMenu.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		}
	}

    private void firstTimeHint() {
        new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(R.id.runmania_musicList, this))
                .setContentTitle("First time user tips")
                .setContentText("Swipe left or right to remove song; long hold to reorder!")
                .hideOnTouchOutside()
                .build();
    }

	public void browse(View view) {
		SimpleFileDialog FileOpenDialog = new SimpleFileDialog(EnterMusic.this,
				initialPath, new SimpleFileDialog.SimpleFileDialogListener() {
			@Override
			public void onChosenDir(String chosenDir) {
				// The code in this function will be executed when the
				// dialog OK button is pushed
				File f = new File(chosenDir);
				if (f.isDirectory()) {
					for (File file : f.listFiles()) {
						if (SimpleFileDialog.fileExtEndsIn(file.getName(), SimpleFileDialog.PERMITTED_EXTENSIONS)) {
							mAdapter.add(System.currentTimeMillis() + "/" + file.getAbsolutePath());
						}
					}
				} else {
					mAdapter.add(System.currentTimeMillis() + "/" + f.getAbsolutePath());
				}

//                firstTimeHint();
			}
		});
		FileOpenDialog.Default_File_Name = "";
		FileOpenDialog.chooseFile_or_Dir();
	}
}
